import os
from datetime import date
from difflib import get_close_matches
import geopandas as gpd
import pandas as pd

def homogeneite_valeurs(df,col,liste_termes):
    """ Cette fonction permet de nettoyer les données de textes en :
    1) Remplaçant les valeurs manquantes par "Non disponible"
    2) Remplaçant les valeurs par une liste prédéfinie de catégories.
    Cette étape utilise la bibliothèque difflib afin de calculer la similarité
    entre les valeurs et les catégories prédéfinies. 
    
    Attention /!\ : Si de nouvelles catégories apparaissent (ex : un nouveau milieu), 
    il doit être rajouté à la liste de termes manuellement sous peine de le voir remplacer lors du nettoyage.

    Arguments:
        df: le dataframe à traiter.
        col: la colonne à traiter.
        liste_termes : La liste des termes (catégories) corrects

    Output:
        df : le Dataframe avec la colonne homogène. 
    """

    df[col].fillna('Non disponible', inplace=True)
    df[col] = [get_close_matches(terme, liste_termes)[0] for terme in df[col]]
    return df


def cleaning(df):
    
    """ Cette fonction permet de nettoyer toutes les données en:
    1) Selectionnant uniquement les signalements validés (détruits ou non)
    2) Homogénéisant les espèces, milieux, densités et ainsi éviter les fautes de frappes 
    ou mauvaises catégories
    3) Créant une colonne de l'année du signalement.
    4) Créant un GeoDataframe en crs=2154
    
    Attention /!\ : Voir l'avertissement de la fonction 'homogeneite_valeurs'
    concernant l'ajout de catégories.'
    

    Arguments:
        df: le dataframe à traiter.


    Output:
        df_clean : Le Dataframe nettoyé
    """
    
    
    # Filtre des valeurs validé (détruite et non détruite)
    list_of_value = ['validé non détruit','validé détruit']
    df.query('`properties.c_statut` in @list_of_value')
    
    # Nettoyage des données : 
    
    # Homogénéinité des espèces
    liste_termes = ['Ambroisie trifide', 
                     "Ambroisie à feuilles d'armoise",
                     'Ambroisie à épis lisses']
    
    # Retrait des valeurs manquantes
    
    df = homogeneite_valeurs(df,"properties.c_espece",liste_termes)
      
    df["properties.c_espece"].replace("Ambroisie à feuilles d`armoise", "Ambroisie à feuilles d'armoise",inplace=True)
    
    ###
    # Homogénéinité des milieux
    
    liste_termes = ['Chantier', 
                     'Route',
                     'Champ', 
                     'Autre', 
                     'Résidentiel/jardin',
                     'Vigne/arboriculture',
                     "Cours d'eau",
                     'Carrière',
                     'Forêt',
                     'Non disponible']
    
    df = homogeneite_valeurs(df,"properties.c_milieu",liste_termes)
       
    
    ###
    # Homogénéinité de la densité
    
    liste_termes = ['Entre 50 et 500', 
                     'Entre 10 et 50', 
                     'Inférieur à 10',
                     'Supérieur à 500',
                     'Non disponible']
    df = homogeneite_valeurs(df,"properties.c_densite",liste_termes)
    
    
    ###
    # Création de la colonne année
    countNA=df.isna().any(axis=1).sum()
    if countNA != 0: print(f"Attention : {countNA} ligne(s) sans date qui a(ont) été retirée(s)" )
    df.dropna(axis=0, inplace=True)
    df["properties.c_date_sb_data_1"]=pd.to_datetime(df["properties.c_date_sb_data_1"])
    df["annee"]=df["properties.c_date_sb_data_1"].dt.strftime('%Y')
    df["properties.c_date_sb_data_1"]=df["properties.c_date_sb_data_1"].astype(str)
    
    return df

def concat_comptage_par_dep(comptage_passe,df_clean):
    
    """ Cette fonction permet compter le nombre de signalement par département
    et l'ajoute aux années précédentes. 
    
    Arguments:
        comptage_passe: le csv précédent > ambroisies_count_department.csv
        df_clean: le dataframe de l'année en cours nettoyé


    Output:
        df_count_updated : le comptage de signalement par département (à sauvegarder par la suite) 
    """
    
    Count_dep=df_clean.groupby(['annee',
                              "properties.c_code_dpt"]).size().reset_index(name='counts')
    Count_dep.rename(
        columns={"properties.c_code_dpt": "c_code_dpt"},
        inplace=True,
    )
    Count_dep["annee"]=Count_dep["annee"].astype('int64')
    
    df_count_updated=pd.concat([comptage_passe, Count_dep]).groupby(['annee', 'c_code_dpt']).sum().reset_index()
    return df_count_updated

def transform_gdf(df_clean):
    
    """ Cette fonction permet de transformer le df (pandas) en geoDataframe en crs=2154,
    créer la variable 'geometry', et filter les colonnes d'intérêts
    
    Arguments:
        df_clean: le dataframe de l'année en cours nettoyé


    Output:
        gdf_light : Le GeoDataframe prêt à être concaténé
    """
        
    gpd.options.display_precision = 8
    gdf = gpd.GeoDataFrame(df_clean, geometry=gpd.points_from_xy(df_clean["properties.c_x"], df_clean["properties.c_y"]),crs=2154)
    
    gdf.rename(
        columns={"properties.c_espece":"espece",
                 "properties.c_date_sb_data_1":"date_signalement",
                 "properties.c_densite":"densite",
                 "properties.c_milieu":"milieu"},
        inplace=True,
    )
    
    gdf_light=gdf[["annee","espece","date_signalement","densite","milieu","geometry"]]
    
    return gdf_light



def main(arg1, arg2,arg3):
    # Importation des données
    colonnes_requises=["properties.c_code_dpt",
                       "properties.c_espece",
                    "properties.c_date_sb_data_1",
                    "properties.c_densite",
                    "properties.c_statut",
                    "properties.c_milieu",
                    "properties.c_x",
                    "properties.c_y"]
    
    df_Geojson = gpd.read_file(arg1)
    df_comptage_dep = pd.read_csv(arg2,sep=';')
    df_new = pd.read_csv(arg3,sep=',', usecols=colonnes_requises)
    print("Importation des données terminée")
    
    # Création d'un dossier d'output à la date du jour
    today = date.today()
    print("Date du jour:", today)
    print("Création du dossier Output:")
    os.makedirs(f'Output_{today}',exist_ok=True) 
    
    # BACKUP du GeoJSON utilisé par la carte
    print("Sauvegarde d'un Back-up des données utilisées pas la carte")
    df_Geojson.to_file(f"Output_{today}/BACKUP_signalements_ambroisies_clean_light_{today}.geojson", driver='GeoJSON')
    df_comptage_dep.to_csv(f"Output_{today}/BACKUP_ambroisies_count_department.csv",index = False, encoding="utf-8", sep=';')
  
    # Nettoyage des données
    print("Nettoyage des données")
    df_new=cleaning(df_new)
    
    # Concaténation des données de comptage par département
    df_comptage_dep=concat_comptage_par_dep(df_comptage_dep,df_new)
    
    
    # Concaténation des données géographiques
    print("Concaténation des données")
    ## Transformation en geodataframe
    df_new=transform_gdf(df_new)
    ## concaténation
    df_Geojson = pd.concat([df_Geojson, df_new])
    
    # Sauvegarde des données
    print("Sauvegarde des données concaténées")
    
    df_Geojson.to_file(f"Output_{today}/signalements_ambroisies_clean_light.geojson", driver='GeoJSON')
    df_comptage_dep.to_csv(f"Output_{today}/ambroisies_count_department.csv",index = False, encoding="utf-8", sep=';')
    print(f"Concaténation terminée : le backup et les données fusionnées sont présentes dans le dossier 'Output_{today}'")  
    
if __name__ == "__main__":
        
    arg1 = input("Indiquez le chemin des données des signalements précédents (signalements_ambroisies_clean_light.geojson). \nVous pouvez glisser le fichier dans la fenêtre et APPUYER SUR ENTRER : \n")
    arg1= r'{}'.format(arg1)
    print(" ")
    arg2 = input("Indiquez le chemin des données de comptage par départements (ex : ambroisies_count_department.csv). \n Vous pouvez glisser le fichier dans la fenêtre et APPUYER SUR ENTRER : \n")
    arg2= r'{}'.format(arg2)
    print(" ")
    arg3 = input("Indiquez le chemin des données de l'année à concaténer (ex : export 2023.csv). \n Vous pouvez glisser le fichier dans la fenêtre et APPUYER SUR ENTRER : \n")
    arg3= r'{}'.format(arg3)
    print(" ")

    main(arg1,arg2,arg3)  
