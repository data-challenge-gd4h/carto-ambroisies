import pandas as pd
import requests
from bs4 import BeautifulSoup
import difflib

# Récupération de la liste des départements concernés par un arrêté préfectoral

lien_page_arretes = "https://ambroisie-risque.info/reglementation/#les-arretes-prefectoraux-par-departement"

def recup_export():
    page_liste_arretes = requests.get(lien_page_arretes)
    soup = BeautifulSoup(page_liste_arretes.content, "html.parser")
    partie_arretes = soup.find(id="les-arretes-prefectoraux-par-departement").find("table")
    tableau_arretes = pd.read_html(str(partie_arretes))[0]
    return tableau_arretes.iloc[1:,0].copy().rename("departements-noms").reset_index(drop=True)

# Comparaison avec la liste complète des départements
# Corrections éventuelles des différences de rédaction
# Ajout des codes département

liste_dpts_avec_arrete = recup_export()

liste_complete_dpts = pd.read_json("listesimplifiee-departements.json")
liste_complete_dpts = pd.json_normalize(liste_complete_dpts["features"])
liste_complete_dpts = liste_complete_dpts[["properties.departements-numeros", "properties.departements-noms"]]
liste_complete_dpts.rename(columns={"properties.departements-numeros": "departements-numeros", "properties.departements-noms": "departements-noms"}, inplace=True)

liste_dpts_avec_arrete = liste_dpts_avec_arrete.apply(lambda x: difflib.get_close_matches(x, liste_complete_dpts["departements-noms"])[0]).to_frame()
liste_dpts_avec_arrete = pd.merge(liste_dpts_avec_arrete, liste_complete_dpts, how="inner", on="departements-noms")

liste_dpts_avec_arrete.to_csv("liste_dpts_avec_arrete.csv", sep=";", index=False)