import pandas as pd
import requests
from bs4 import BeautifulSoup

url_page = 'https://www.insee.fr/fr/information/2549968'
url_base = 'https://www.insee.fr'

def scrape_insee() -> pd.DataFrame:
    """
    Procède à l'extraction de la table des communes qui ont fait l'objet
    d'un changement de code insee au cours de l'année N-1 en raison
    d'une procédure de regoupement ou de fusion prévue
    par la loi du 08 novembre 2016.

    Cette fonction prend en entrée l'url de la page du site de l'INSEE
    et retourne un DataFrame contenant le code insee de l'ancienne commune
    et celui de la nouvelle commune née du regroupement.

    Paramètres:
    ----------
    url_page: str
        le lien de la page où se trouve le fichier au format xlsx
        qui doit être importé.

    url_base: str
        l'adresse url du site de l'INSEE.

    Retourne:
    --------
    Un DataFrame à  deux colonnes.
    """
    r = requests.get(url_page)
    soup = BeautifulSoup(r.text, "html.parser")
    anchors = soup.find_all('a', {'class': 'fichier'})
    url_fichier = f'{url_base}{anchors[0].get("href")}'
    df = pd.read_excel(url_fichier,
                       dtype={"DepComN": str, "DepComA": str}
                       )[["DepComN", "DepComA"]]

    return df

def referents_ambroisie_update() -> pd.DataFrame:
    """
    Procède à la mise à jour - à faire en janvier/février -
    du fichier anonymisé des référents ambroisie.

    Cette fonction prend en entrée les codes insee
    du DataFrame `referents_ambroisie_2023`
    et retourne une nouvelle version de ce DataFrame initial.

    La modification consiste à identifier les valeurs
    communes aux deux datasets - listes `code_insee` et `DepComA` -,
    et à les remplacer, dans le DataFrame original,
    par les valeurs de `DepComN`, la colonne des nouveaux codes.

    Paramètres:
    ----------
    df1: pd.DataFrame
        le DataFrame contenant le code insee
        des communes en France métropolitaine
        où l'on peut localiser/contacter un référent ambroisie.

     df2: pd.DataFrame
        le DataFrame contenant la table des communes créées au cours de l'année N-1
        avec leurs anciens et nouveaux codes insee.

    Retourne:
    --------
    Un nouveau DataFrame au format csv.
    """
    df1 = pd.read_csv('data/referents_ambroisie_2023.csv',
                      sep=";",
                      dtype={"code_insee": str},
                      encoding='utf-8'
                      ).sort_values('code_insee')
    df2 = scrape_insee()

    # conversion en liste pour effectuer une évaluation des contenus
    old_codes_df1, old_codes_df2 = df1['code_insee'].tolist(), df2['DepComA'].tolist()

    if not df1.query('code_insee in @old_codes_df2').empty:
        # sélection des indices des lignes qui doivent être mises à jour
        idx_codes_to_update = df1.query('code_insee in @old_codes_df2').index.values
        # sélection des indices des lignes qui contiennent les valeurs de remplacement
        # (à extraire du dataframe df2, le dataset des communes nouvellement créées)
        idx_new_codes = df2.query('DepComA in @old_codes_df1').sort_values('DepComA').index.values
        # affectation des nouvelles valeurs
        df1.loc[idx_codes_to_update, 'code_insee'] = df2.loc[idx_new_codes, 'DepComN'].values
        df1 = (df1
               .drop_duplicates()
               .reset_index(drop=True)
               )
    else:
        pass

    df1.to_csv("referents_ambroisie_2023.csv",
               index=False,
               sep=";",
               encoding="utf-8"
               )


referents_ambroisie_update()
