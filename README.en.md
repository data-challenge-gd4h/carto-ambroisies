# Challenge GD4H - Carto'Ambroisies

<a href="https://gd4h.ecologie.gouv.fr/" target="_blank" rel="noreferrer">Green Data for Health</a> (GD4H) is an initiative led by the Innovation Lab (Ecolab) of the French ministry of ecology.

A challenge has been organised in 2023 to develop tools, rooted in the health-environment data community, aiming at addressing shared issues.
 
<a href="https://gd4h.ecologie.gouv.fr/defis" target="_blank" rel="noreferrer">Website</a>

## Carto'Ambroisies

Ragweeds are very invasive plants. Once an ambrosia plant is observed, it must be quickly eliminated because it is difficult to eradicate once it is installed.

Moreover, according to ANSES, nearly 3% of the French population is allergic to Ambrosia. The pollen of this plant can, in late summer, cause strong allergic reactions.

Also, its ability to produce large quantities of seeds makes it a threat to agriculture and biodiversity.

## **Documentation**

Carto'Ambroisies is an interactive mapping of data on ragweed in France.

The backend folder contains different scripts for retrieving and processing data from different sources.

The frontend folder contains the code for the interactive map and web application.

This project generates an interactive map available via gitlab pages, and also integrated into this page: https://ambroisie-risque.info/test-data-challenge/


### **Installation**

[Installation Guide](/INSTALL.md)

### **Contributing**

If you wish to conribute to this project, please follow the [recommendations](/CONTRIBUTING.md).

### **Licence**

The code is published under [MIT Licence](/LICENSE).

The data referenced in this README and in the installation guide is published under <a href="https://www.etalab.gouv.fr/wp-content/uploads/2018/11/open-licence.pdf">Open Licence 2.0</a>.
