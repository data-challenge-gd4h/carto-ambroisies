# vue3-leaflet-poc

## Périmètre :

Affichage d'une carte Leaflet
Zoom à la sélection d'une commune (via API https://api-adresse.data.gouv.fr/search/ )
## Tester avec

node versin : 16.13
npm : 8.1.3

utilisation des composants vuetify

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Appel API

Les appels api se font dans le répertoire api 

Accès à l'API altasanté non appelé.