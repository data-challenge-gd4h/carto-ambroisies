import * as L from "leaflet";
import "./logoCustomControl.css";
import {
  iconeArmoise,
  iconeTrifide,
  iconeEpislisses,
} from "./icon/ambroisieIcon";

export class LogoCustomControl extends L.Control {
  // Si nécessaire d'accéder à la map
  // onAdd(map: L.Map) {
  onAdd() {
    const div = L.DomUtil.create("div");
    div.classList.add("container-legende");

    const legendeSignalements = document.createElement("div");

    const title = document.createElement("h1");
    title.innerText = "Légende";
    legendeSignalements.appendChild(title);

    function ajouterEltLegende(nomIcone: L.DivIcon, intitule: string) {
      const ligneElt = document.createElement("div");
      ligneElt.style.display = "flex";
      ligneElt.style.margin = "8px";
      const divIcone = document.createElement("div");
      divIcone.style.display = "inline-block";
      divIcone.classList.add("icone-signalement");
      if (nomIcone.options.iconSize !== undefined) {
        divIcone.style.width =
          nomIcone.options.iconSize.toString().split(",")[0] + "px";
        divIcone.style.height =
          nomIcone.options.iconSize.toString().split(",")[1] + "px";
      }
      if (typeof nomIcone.options.html == "string") {
        divIcone.innerText = nomIcone.options.html;
      }
      ligneElt.appendChild(divIcone);
      const intituleSpan = document.createElement("span");
      intituleSpan.innerText = intitule;
      intituleSpan.style.lineHeight = "24px";
      intituleSpan.style.marginLeft = "7px";
      divIcone.after(intituleSpan);
      legendeSignalements.appendChild(ligneElt);
    }

    const ligneEltCluster = document.createElement("div");
    ligneEltCluster.style.display = "flex";
    ligneEltCluster.style.margin = "6px";
    const divIconeCluster = document.createElement("div");
    divIconeCluster.style.display = "inline-block";
    divIconeCluster.classList.add("icone-cluster");
    divIconeCluster.style.width = "30px";
    divIconeCluster.style.height = "30px";
    divIconeCluster.innerText = "42";
    ligneEltCluster.appendChild(divIconeCluster);
    const intituleCluster = document.createElement("span");
    intituleCluster.innerText =
      "Nombre de signalements aux alentours (année en cours)";
    intituleCluster.style.lineHeight = "30px";
    intituleCluster.style.marginLeft = "3px";
    divIconeCluster.after(intituleCluster);
    legendeSignalements.appendChild(ligneEltCluster);

    ajouterEltLegende(iconeArmoise, "Ambroisie à feuille d’armoise");
    ajouterEltLegende(iconeTrifide, "Ambroisie trifide");
    ajouterEltLegende(iconeEpislisses, "Ambroisie à épis lisses");

    div.appendChild(legendeSignalements);

    const blocLogos = document.createElement("div");
    blocLogos.style.display = "inline-block";
    blocLogos.style.position = "absolute";
    blocLogos.style.right = "0";
    blocLogos.style.bottom = "10px";

    const imgFredon = document.createElement("img");
    imgFredon.src = require("@/assets/logo-fredon.png");
    imgFredon.style.width = "70px";
    blocLogos.appendChild(imgFredon);

    const imgObservatoire = document.createElement("img");
    imgObservatoire.src = require("@/assets/logo-observatoire-ambroisie.png");
    imgObservatoire.style.width = "110px";
    blocLogos.appendChild(imgObservatoire);

    div.appendChild(blocLogos);

    return div;
  }

  // Fonction non nécessaire aujourd'hui. A réactiver si les besoins évoluent
  // onRemove(map: L.Map) {
  //   // Nothing to do here
  //   console.log("onRemove");
  // }
  constructor(options?: L.ControlOptions) {
    super(options);
  }
}
