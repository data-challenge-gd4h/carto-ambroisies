import L from "leaflet";
import "../logoCustomControl.css";

const iconSize: [number, number] = [25, 25];
const iconAnchor: [number, number] = [16, 32];
const popupAnchor: [number, number] = [0, -40];

export const iconeArmoise = L.divIcon({
  className: "icone-signalement",
  iconSize: iconSize,
  iconAnchor: iconAnchor,
  popupAnchor: popupAnchor,
  html: "A",
});

export const iconeTrifide = L.divIcon({
  className: "icone-signalement",
  iconSize: iconSize,
  iconAnchor: iconAnchor,
  popupAnchor: popupAnchor,
  html: "T",
});

export const iconeEpislisses = L.divIcon({
  className: "icone-signalement",
  iconSize: iconSize,
  iconAnchor: iconAnchor,
  popupAnchor: popupAnchor,
  html: "P",
});

export const iconeArmoiseHisto = L.divIcon({
  className: "icone-signalement-histo",
  iconSize: iconSize,
  iconAnchor: iconAnchor,
  popupAnchor: popupAnchor,
  html: "A",
});

export const iconeTrifideHisto = L.divIcon({
  className: "icone-signalement-histo",
  iconSize: iconSize,
  iconAnchor: iconAnchor,
  popupAnchor: popupAnchor,
  html: "T",
});

export const iconeEpislissesHisto = L.divIcon({
  className: "icone-signalement-histo",
  iconSize: iconSize,
  iconAnchor: iconAnchor,
  popupAnchor: popupAnchor,
  html: "P",
});
