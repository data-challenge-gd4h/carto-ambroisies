/* eslint-disable @typescript-eslint/no-explicit-any */

export class PreviousObservationByDepartments {
  public annee: number;
  public c_code_dpt: number;
  public counts: number;

  constructor(json: any) {
    this.annee = json.annee;
    this.c_code_dpt = json.c_code_dpt;
    this.counts = json.counts;
  }
}
