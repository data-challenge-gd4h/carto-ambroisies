/* eslint-disable @typescript-eslint/no-explicit-any */
export class PresenceReferent {
  public code_insee: string;

  constructor(json: any) {
    this.code_insee = json.code_insee;
  }
}
