/* eslint-disable @typescript-eslint/no-explicit-any */
export class risquePollen {
  public countyNumber: number;
  public countyName: string;
  public Ambroisies: number;
  public AmbroisiesRandomSample: number; // pour les tests

  constructor(json: any) {
    this.countyNumber = json.countyNumber;
    this.countyName = json.countyName;
    this.Ambroisies = json.Ambroisies;
    this.AmbroisiesRandomSample = json.AmbroisiesRandomSample;
  }
}
