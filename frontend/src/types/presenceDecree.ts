/* eslint-disable @typescript-eslint/no-explicit-any */
export class PresenceDecree {
  public departements_numeros: number;

  constructor(json: any) {
    this.departements_numeros = json.departements_numeros;
  }
}
