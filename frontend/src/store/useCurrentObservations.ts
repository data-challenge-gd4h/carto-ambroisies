import { ref } from "vue";
import { defineStore } from "pinia";
import { xhrCurrentObservations } from "@/api/atlasante";
import {
  iconeArmoise,
  iconeTrifide,
  iconeEpislisses,
} from "@/leaflet/icon/ambroisieIcon";
import L from "leaflet";
import proj4 from "proj4";
// Les éléments proj4leaflet sont-ils nécessaires ?
import "proj4leaflet";

export const useCurrentsObservationStore = defineStore(
  "currentObservations",
  () => {
    const minZoom = 1;
    const maxZoom = 19;
    const isloaded = ref(false);
    const layer = ref();
    const data = ref();
    const load = () => {
      xhrCurrentObservations().then((res) => {
        isloaded.value = false;

        proj4.defs(
          "EPSG:2154",
          "+proj=lcc +lat_0=46.5 +lon_0=3 +lat_1=49 +lat_2=44 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs +type=crs"
        );

        layer.value = L.Proj.geoJson(res, {
          pointToLayer: function (feature, latlng) {
            if (feature.properties.c_espece.includes("armoise")) {
              return new L.Marker(latlng, {
                icon: iconeArmoise,
              }) as L.Layer;
            } else if (feature.properties.c_espece.includes("trifide")) {
              return new L.Marker(latlng, {
                icon: iconeTrifide,
              }) as L.Layer;
            } else {
              return new L.Marker(latlng, {
                icon: iconeEpislisses,
              }) as L.Layer;
            }
          },
          onEachFeature: function (feature, layer) {
            layer.bindPopup(
              "<h3>" +
                feature.properties.c_espece +
                "</h3>" +
                "<p>Densité : " +
                feature.properties.c_densite +
                "</p>" +
                "<p>Milieu : " +
                feature.properties.c_milieu +
                "</p>" +
                "<p>Signalé le " +
                new Date(
                  feature.properties.c_date_sb_data_1
                ).toLocaleDateString("fr") +
                "</p>"
            );
          },
        });
        isloaded.value = true;
      });
    };
    return {
      minZoom,
      maxZoom,
      load,
      isloaded,
      layer,
      data,
    };
  }
);
