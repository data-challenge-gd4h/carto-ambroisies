/* eslint-disable @typescript-eslint/no-explicit-any */
import { FeatureCollection } from "geojson";

export const fetchDepartments = async (): Promise<FeatureCollection> => {
  return await fetch(process.env.BASE_URL + "data/departments.json", {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
  })
    .then((response) => {
      if (!response.ok) {
        throw response;
      }
      if (response.status !== 204) {
        return response.json();
      } else {
        return null;
      }
    })
    .then((response) => {
      return response;
    });
};
