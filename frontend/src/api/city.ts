/* eslint-disable @typescript-eslint/no-explicit-any */
import { urlParamsHelper } from "@/helper/urlParamsHelper";
import { City } from "@/types/city";
import { Feature } from "geojson";

export const getCities_By_Name = async (params: any): Promise<[]> => {
  // URL
  const fullUrl = new URL("https://api-adresse.data.gouv.fr/search/");
  params.q = encodeURI(params.q);
  params.type = "municipality";
  params.limit = 10;
  params.autocomplete = 1;

  if (params) fullUrl.search = urlParamsHelper(params).toString();
  return await fetch(fullUrl.toString(), {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
  })
    .then((response) => {
      if (!response.ok) {
        throw response;
      }
      if (response.status !== 204) {
        return response.json();
      } else {
        return null;
      }
    })
    .then((response) => {
      const retour = response.features.map((item: Feature) => {
        const a = new City(item.properties, item.geometry);
        return a;
      });
      return retour;
    });
};

export const getCity_By_Coordinates = async (
  lat: number,
  lon: number,
  params: any
): Promise<City | null> => {
  // URL
  const fullUrl = new URL("https://geo.api.gouv.fr/communes"); //?lat=48.579734&lon=-4.412597&fields=codeDepartement,departement&format=geojson&geometry=contour
  params.fields = "departement,region";
  params.format = "geojson";
  params.geometry = "contour";
  params.lat = lat;
  params.lon = lon;

  if (params) fullUrl.search = urlParamsHelper(params).toString();
  return await fetch(fullUrl.toString(), {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
  })
    .then((response) => {
      if (!response.ok) {
        throw response;
      }
      if (response.status !== 204) {
        return response.json();
      } else {
        return null;
      }
    })
    .then((response) => {
      if (response.features && response.features.length > 0) {
        const city = new City(
          response.features[0].properties,
          null,
          null,
          response
        );
        return city;
      }
      return null;
      //return response;
    });
};
