# Installation Guide

## Backend

### Dependencies

```bash
cd backend
pip install -r requirements.txt
```

## Frontend

Tested with :

- node : 16.13
- npm : 8.1.3

### Dependencies

You can install dependencies to run the project locally with :

```bash
cd frontend
npm install
```

### Development environment

This will compiles and hot-reloads for development :

```bash
npm run serve
```

### Production environment

This will compiles and minifies for production :

```bash
npm run build
```

### Linting

To run the linter :

```bash
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
